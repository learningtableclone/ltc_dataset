# LTC_dataset

LTC_dataset is the dataset used in *Learning to Detect Table Clones in Spreadsheets*. We publish it here for those who want to learn more about our paper or use it for further research.

## Overview

```bash
.
├── Ground_truth
│   ├── EUSES
│   │   ├── EUSES-all.xlsx	# Statistics of EUSES_dataset
│   │   └── EUSES_dataset	# 50 spreadsheets taken from EUSES corpus
│   └── Enron
│       ├── Enron-all.xlsx	# Statistics of Enron_dataset
│       └── Enron_dataset	# 50 spreadsheets taken from Enron corpus
└── Test
    ├── EUSES
    │   ├── EUSES_100_dataset	# 100 spreadsheets taken from EUSES corpus
    │   └── test-euses-100.xlsx	# The results of sampled 200 table clone pairs.
    ├── Enron
    │   ├── Enron_100_dataset	# 100 spreadsheets taken from Enron corpus
    │   └── test-enron-100.xlsx	# The results of sampled 200 table clone pairs.
    └── FUSE
        ├── FUSE_100_dataset	# 100 spreadsheets taken from FUSE corpus
        └── test-fuse-100.xlsx	# The results of sampled 200 table clone pairs.
```

## Details

LTC_dataset includes two main folders:

* Ground_truth (The ground truth we used in our experiments)
* Test (The test data we used in our experiments)

### Folder 'Ground_truth'

Two main folders here:

* EUSES

  * EUSES-all.xlsx

    The statistics of EUSES_dataset including AllTable, Clone and NonClone information.

  * EUSES_dataset

    There are 50 spreadsheets taken from EUSES corpus in it.

* Enron

  * Enron-all.xlsx

    The statistics of Enron_dataset including AllTable, Clone and NonClone information.

  * Enron_dataset

    There are 50 spreadsheets taken from Enron corpus in it.

#### Explanation

EUSES-all.xlsx and Enron-all.xlsx contain three worksheets, respectively.

* AllTable

  This worksheet contains the information of all tables found in EUSES_dataset / Enron_dataset.

* Clone

  This worksheet contains the information of all table clone pairs found in EUSES_dataset / Enron_dataset.

* NonClone

  This worksheet contains the information of all non-clone pairs in EUSES_dataset / Enron_dataset.

Details of the header in EUSES-all.xlsx and Enron-all.xlsx:

* Category

  The category of the spreadsheet in EUSES corpus. (Enron corpus does not have categories.)

* Spreadsheet

  The spreadsheet name of the table.

* Table

  The region of the table.

### Folder 'Test'

* EUSES

  * test-euses-100.xlsx

    The manually validate results of sampled 100 inter-spreadsheet table clones and 100 intra-spreadsheet table clones.

  * EUSES_100_dataset

    There are 100 spreadsheets taken from EUSES corpus in it.

* Enron

  * test-enron-100.xlsx:

    The manually validate results of sampled 100 inter-spreadsheet table clones and 100 intra-spreadsheet table clones.

  * Enron_100_dataset

    There are 100 spreadsheets taken from Enron corpus in it.

* FUSE

  * test-fuse-100.xlsx:

    The manually validate results of sampled 100 inter-spreadsheet table clones and 100 intra-spreadsheet table clones.

  * FUSE_100_dataset

    There are 100 spreadsheets taken from FUSE corpus in it.

#### Explanation

test-euses-100.xlsx, test-enron-100.xlsx and test-fuse-100.xlsx contain two worksheets, respectively.

* intra-spreadsheet

  This worksheet contains the information of 100 table pairs found in the dataset with the same folder, and the two table of each table pair are in the same spreadsheet.

* inter-spreadsheet

  This worksheet contains the information of 100 table pairs found in the dataset with the same folder, and the two table of each table pair are in different spreadsheets.

Details of the header in test-euses-100.xlsx, test-enron-100.xlsx and test-fuse-100.xlsx:

* Spreadsheet1

  The spreadsheet name of table1.

* Worksheet1

  The worksheet name of table1.

* Table1

  The region of table1.

* Spreadsheet2

  The spreadsheet name of table2.

* Worksheet2

  The worksheet name of table2.

* Table2

  The region of table2.

* Judge

  The manually validate result of  the table clone pair. "0" means FALSE table clone, and "1" means TRUE table clone.

## Reference Paper

Learning to Detect Table Clones in Spreadsheets. *Yakun Zhang, Wensheng Dou, Jiaxin Zhu, Liang Xu, Zhiyong Zhou, Jun Wei, Dan Ye, Bo Yang*. 29th ACM SIGSOFT International Symposium on Software Testing and Analysis (ISSTA 2020).

